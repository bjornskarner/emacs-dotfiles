(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;; '(custom-enabled-themes (quote (solarized-light)))
 '(custom-safe-themes (quote ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" default)))
 '(custom-theme-load-path (quote ("/home/bjoska/.emacs.d/elpa/color-theme-solarized-20120301/" custom-theme-directory t))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(load-theme 'deeper-blue t)

;; Auto revert files when they have changed.
(global-auto-revert-mode 1)

(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; Add in your own as you wish:
(defvar my-packages '(starter-kit starter-kit-lisp starter-kit-bindings starter-kit-ruby)
  "A list of packages to ensure are installed at launch.")

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))
;; To Init MMM mode.
;;(require 'mmm-auto)
;;(setq mmm-global-mode 'maybe)
;;(mmm-add-mode-ext-class 'html-mode "\\.erb\\'" 'ruby)

(electric-pair-mode +1)
(column-number-mode +1)
(linum-mode 1)
(global-hl-line-mode 0)
(set-face-background 'region "dark green")

(setq bjoska-code-modes-hook nil)

;; Show whitespace
(add-hook 'bjoska-code-modes-hook
          (lambda () (whitespace-mode +1)))

;; Activate Ruby End Mode
(add-hook 'bjoska-code-modes-hook
          (lambda () (ruby-end-mode t)))

;; Turn on highlight line mode.
(add-hook 'bjoska-code-modes-hook
          (lambda () (hl-line-mode 1)
            (set-face-background hl-line-face "OrangeRed")
            (set-face-background region "dark green")
            ))

(add-hook 'ruby-mode-hook
          (lambda () (run-hooks 'bjoska-code-modes-hook)))

;; Custom functions
;; Mostly stuff from Emacs Redux.
;;;;;;;;;;;;;;;;;;;

(defun bjoska-iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(defun bjoska-indent-buffer ()
  "Indent the currently visited buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(defun bjoska-indent-region-or-buffer ()
  "Indent a region if selected, otherwise the whole buffer."
  (interactive)
  (save-excursion
    (if (region-active-p)
        (progn
          (indent-region (region-beginning) (region-end))
          (message "Indented selected region."))
      (progn
        (indent-buffer)
        (message "Indented buffer.")))))

;; This should be available in prelude, should look into this.
(defun bjoska-indent-defun ()
  "Indent the current defun."
  (interactive)
  (save-excursion
    (mark-defun)
    (indent-region (region-beginning) (region-end))))

(defun bjoska-duck-d-g ()
  "DuckDuck the selected region if any, display a query prompt otherwise."
  (interactive)
  (browse-url
   (concat
    "http://www.duckduckgo.com/?q="
    (url-hexify-string (if mark-active
         (buffer-substring (region-beginning) (region-end))
       (read-string "DuckDuckGo: "))))))

(defun visit-term-buffer ()
  "Create or visit a terminal buffer."
  (interactive)
  (if (not (get-buffer "*ansi-term*"))
      (progn
        (split-window-sensibly (selected-window))
        (other-window 1)
        (ansi-term (getenv "SHELL")))
    (switch-to-buffer-other-window "*ansi-term*")))

(defun bjoska-move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun bjoska-move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

;; Custom Keyboard Bindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Just in place to ensure I stop using the arrow keys.
(global-unset-key (kbd "<left>"))
(global-unset-key (kbd "<right>"))
;; (global-unset-key (kbd "<up>"))
;; (global-unset-key (kbd "<down>"))

;; Slowly resetting everything.
(global-set-key (kbd "<up>") 'previous-line)
(global-set-key (kbd "<down>") 'next-line)

;; Moving lines up and down.
(global-set-key [(meta shift up)]  'bjoska-move-line-up)
(global-set-key [(meta shift down)]  'bjoska-move-line-down)
;; (global-set-key [(control shift up)]  'bjoska-move-line-up)
;; (global-set-key [(control shift down)]  'bjoska-move-line-down)

;; In place to prevent me from closing emacs.
(global-unset-key (kbd "C-x C-c")) 
(global-unset-key (kbd "C-x C-z"))

;; There seems to be an issue using english layout on a swedish
;; keyboard, this will ensure that the comment-region command works.
(global-set-key (kbd "<f12>") 'comment-or-uncomment-region)
(global-set-key (kbd "M-ö") 'comment-dwim)

;; Changing hippie-expand binding from M-/ to <ESC>
(global-set-key (kbd "<escape>") 'hippie-expand)

;; Indent function
(global-set-key (kbd "C-M-z") 'indent-defun)

;; Visit terminal buffer
(global-set-key (kbd "C-c t") 'visit-term-buffer)
