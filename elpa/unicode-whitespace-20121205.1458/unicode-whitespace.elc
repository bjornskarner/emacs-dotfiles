;ELC   
;;; Compiled by bjoska@bjoska-ThinkPad-W510 on Fri Jan 25 15:16:17 2013
;;; from file /home/bjoska/.emacs.d/elpa/unicode-whitespace-20121205.1458/unicode-whitespace.el
;;; in Emacs version 24.1.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'whitespace)
#@57 Return the character corresponding to NAME, a UCS name.
(autoload 'ucs-utils-char "ucs-utils" '(#$ . 587))
#@71 Return a string corresponding to SEQUENCE of UCS names or characters.
(autoload 'ucs-utils-string "ucs-utils" '(#$ . 700))
#@40 Return a prettified UCS name for CHAR.
(autoload 'ucs-utils-pretty-name "ucs-utils" '(#$ . 829))
#@67 Return the first existing element in SEQUENCE of character names.
(autoload 'ucs-utils-first-existing-char "ucs-utils" '(#$ . 932))
#@47 Hold the value of `show-trailing-whitespace'.
(defvar unicode-whitespace-saved-show-trailing nil (#$ . 1070))
(make-variable-buffer-local 'unicode-whitespace-saved-show-trailing)
#@41 Track the point to avoid needless echo.
(defvar unicode-whitespace-last-point nil (#$ . 1255))
(make-variable-buffer-local 'unicode-whitespace-last-point)
#@41 Regexp matching any type of whitespace.
(defvar unicode-whitespace-all-whitespace-regexp nil (#$ . 1416))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-empty 'unicode-whitespace-subdued-empty (#$ . 1528))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-trailing 'unicode-whitespace-subdued-trailing (#$ . 1661))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-indentation 'unicode-whitespace-subdued-indentation (#$ . 1800))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-space-after-tab 'unicode-whitespace-subdued-space-after-tab (#$ . 1945))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-space-before-tab 'unicode-whitespace-subdued-space-before-tab (#$ . 2098))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-tab 'unicode-whitespace-subdued-tab (#$ . 2253))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-space 'unicode-whitespace-subdued-space (#$ . 2382))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-hspace 'unicode-whitespace-subdued-hspace (#$ . 2515))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-newline 'unicode-whitespace-subdued-newline (#$ . 2650))
#@40 Face variable to make font-lock happy.
(defvar unicode-whitespace-subdued-line 'unicode-whitespace-subdued-line (#$ . 2787))
(byte-code "\300\301\302\303\304\305\306\307\306\310\306\311\312\313\314\315\314\316&\210\300\317\302\320\314\301%\207" [custom-declare-group unicode-whitespace nil "Teach whitespace-mode about fancy characters." :version "0.2.3" :link (emacs-commentary-link :tag "Commentary" "unicode-whitespace") (url-link :tag "Github" "http://github.com/rolandwalker/unicode-whitespace") (url-link :tag "EmacsWiki" "http://emacswiki.org/emacs/UnicodeWhitespace") :prefix "unicode-whitespace-" :group i18n faces unicode-whitespace-definitions "Define character groups recognized by unicode-whitespace"] 18)
#@53 Names of tab characters used by unicode-whitespace.
(custom-declare-variable 'unicode-whitespace-tab-names ''("Character Tabulation With Justification" "Character Tabulation") '(#$ . 3513) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@57 Names of tab-set characters used by unicode-whitespace.
(custom-declare-variable 'unicode-whitespace-tab-set-names ''("Character Tabulation Set" "Line Tabulation Set") '(#$ . 3771) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@60 Names of soft space characters used by unicode-whitespace.
(custom-declare-variable 'unicode-whitespace-soft-space-names ''("Space" "Em Quad" "Em Space" "En Quad" "En Space" "Figure Space" "Four-Per-Em Space" "Hair Space" "Ideographic Space" "Medium Mathematical Space" "Mongolian Vowel Separator" "Ogham Space Mark" "Punctuation Space" "Six-Per-Em Space" "Tag Space" "Thin Space" "Three-Per-Em Space" "Zero Width Space") '(#$ . 4021) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@75 Names of hard (non-breaking) space characters used by unicode-whitespace.
(custom-declare-variable 'unicode-whitespace-hard-space-names ''("Narrow No-Break Space" "No-Break Space" "Word Joiner" "Zero Width No-Break Space") '(#$ . 4525) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@218 Names of pseudo-space characters used by unicode-whitespace.

Pseudo-spaces are treated as space for the purpose of
visualization by this library, but not officially defined as
space in the Unicode 6.1 specification.
(custom-declare-variable 'unicode-whitespace-pseudo-space-names ''("Combining Grapheme Joiner" "Tifinagh Consonant Joiner" "Zero Width Joiner" "Zero Width Non-Joiner") '(#$ . 4831) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@215 Names of line-terminator characters used by unicode-whitespace.

By default, only "Line Feed (LF)" is included here.  Alternative line
terminators are set in `unicode-whitespace-alternative-line-terminator-names'.
(custom-declare-variable 'unicode-whitespace-standard-line-terminator-names ''("Line Feed (LF)") '(#$ . 5299) :type '(repeat string) :group 'unicode-whitespace-definitions)
#@268 Names of alternative line-terminator characters used by unicode-whitespace.

"Line Feed (LF)" should not be included in this list of
alternative terminators; LF is found in the list of standard line
terminators at `unicode-whitespace-standard-line-terminator-names'.
(custom-declare-variable 'unicode-whitespace-alternative-line-terminator-names ''("Carriage Return (CR)" "Line Separator" "Line Tabulation" "Next Line (NEL)" "Paragraph Separator") '(#$ . 5693) :type '(repeat string) :group 'unicode-whitespace-definitions)
(custom-declare-group 'unicode-whitespace-mappings nil "Mappings for characters to display instead of whitespace." :group 'unicode-whitespace)
#@231 Display substitutions for soft space characters when `whitespace-mode' is on.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-soft-space-mappings ''(("Space" ("Middle Dot" ".")) ("Em Quad" ("Circled Bullet" "Middle Dot" ".")) ("Em Space" ("Circled Bullet" "Middle Dot" ".")) ("En Quad" ("Circled Bullet" "Middle Dot" ".")) ("En Space" ("Circled Bullet" "Middle Dot" ".")) ("Figure Space" ("Circled Bullet" "Middle Dot" ".")) ("Four-Per-Em Space" ("Circled Bullet" "Middle Dot" ".")) ("Hair Space" ("Circled Bullet" "Middle Dot" ".")) ("Ideographic Space" ("Circled Bullet" "Middle Dot" ".")) ("Medium Mathematical Space" ("Circled Bullet" "Middle Dot" ".")) ("Mongolian Vowel Separator" ("Circled Bullet" "Middle Dot" ".")) ("Ogham Space Mark" ("Circled Bullet" "Middle Dot" ".")) ("Punctuation Space" ("Circled Bullet" "Middle Dot" ".")) ("Six-Per-Em Space" ("Circled Bullet" "Middle Dot" ".")) ("Tag Space" ("Circled Bullet" "Middle Dot" ".")) ("Thin Space" ("Circled Bullet" "Middle Dot" ".")) ("Three-Per-Em Space" ("Circled Bullet" "Middle Dot" ".")) ("Zero Width Space" ("Circled Bullet" "Middle Dot" "."))) '(#$ . 6367) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-soft-space-names :group 'unicode-whitespace-mappings)
#@231 Display substitutions for hard space characters when `whitespace-mode' is on.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-hard-space-mappings ''(("Narrow No-Break Space" ("Black Diamond" "Middle Dot" ".")) ("No-Break Space" ("Black Diamond" "Middle Dot" ".")) ("Word Joiner" ("Black Diamond" "Middle Dot" ".")) ("Zero Width No-Break Space" ("Black Diamond" "Middle Dot" "."))) '(#$ . 7833) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-hard-space-names :group 'unicode-whitespace-mappings)
#@233 Display substitutions for pseudo-space characters when `whitespace-mode' is on.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-pseudo-space-mappings ''(("Combining Grapheme Joiner" ("Black Medium Small Square" "Middle Dot" ".")) ("Tifinagh Consonant Joiner" ("Black Medium Small Square" "Middle Dot" ".")) ("Zero Width Joiner" ("Black Medium Small Square" "Middle Dot" ".")) ("Zero Width Non-Joiner" ("Black Medium Small Square" "Middle Dot" "."))) '(#$ . 8566) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-pseudo-space-names :group 'unicode-whitespace-mappings)
#@228 Display substitutions for newline characters when `whitespace-mode' is on.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-standard-line-terminator-mappings ''(("Line Feed (LF)" ("Paragraph Sign" "$"))) '(#$ . 9370) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-standard-line-terminator-names :group 'unicode-whitespace-mappings)
#@240 Display substitutions for alternative line-terminator characters in `whitespace-mode'.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-alternative-line-terminator-mappings ''(("Carriage Return (CR)" ("Downwards Arrow With Corner Leftwards" "")) ("Next Line (NEL)" ("Downwards Arrow" "$")) ("Line Separator" ("Downwards Arrow" "$")) ("Paragraph Separator" ("Downwards Arrow" "$")) ("Line Tabulation" ("Downwards Arrow" "$"))) '(#$ . 9939) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-alternative-line-terminator-names :group 'unicode-whitespace-mappings)
#@420 Display substitutions for tab characters in `whitespace-mode'.

Tab characters are already highlighted with a face.  It is best
not to activate mappings here for the standard Tab character,
because (due to a limitation of Emacs) indentation will
sometimes be broken.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-tab-mappings ''(("Character Tabulation With Justification" ("Black Rightwards Arrowhead" "."))) '(#$ . 10734) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-tab-names :group 'unicode-whitespace-mappings)
#@220 Display substitutions for tab-set characters in `whitespace-mode'.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-tab-set-mappings ''(("Character Tabulation Set" ("Rightwards Arrow To Bar" ".")) ("Line Tabulation Set" ("Downwards Arrow To Bar" "."))) '(#$ . 11491) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options unicode-whitespace-tab-set-names :group 'unicode-whitespace-mappings)
#@349 Display substitutions for form-feed characters in `whitespace-mode'.

This mapping is treated specially: if the name matches the
string "box drawing", it is extended to a series of 20
characters.

Each character name is tried in turn until a displayable character
is found.  Character names may be full UCS names or a string of
a single character.
(custom-declare-variable 'unicode-whitespace-form-feed-mappings ''(("Form Feed (FF)" ("Box Drawings Heavy Horizontal" "\f"))) '(#$ . 12093) :type '(alist :key-type string :value-type (group (repeat :tag "Character Names" (string :tag "")))) :options '("Form Feed (FF)") :group 'unicode-whitespace-mappings)
(byte-code "\300\301\302\303\304\305%\210\306\307\310\311\304\301%\210\306\312\313\314\304\301%\210\306\315\316\317\304\301%\210\306\320\321\322\304\301%\210\306\323\324\325\304\301%\210\306\326\327\330\304\301%\210\306\331\332\333\304\301%\210\306\334\335\336\304\301%\210\306\337\340\341\304\301%\210\306\342\343\344\304\301%\207" [custom-declare-group unicode-whitespace-faces nil "Faces for `unicode-whitespace'." :group unicode-whitespace custom-declare-face unicode-whitespace-subdued-empty ((((background dark)) (:background "LightYellow" :foreground "Black")) (((background light)) (:background "LightGoldenrod2" :foreground "Black"))) "Unicode-whitespace face for empty lines at end and beginning of buffer." unicode-whitespace-subdued-trailing ((((background dark)) (:inherit unicode-whitespace-subdued-empty)) (((background light)) (:inherit unicode-whitespace-subdued-empty))) "Unicode-whitespace face for trailing space." unicode-whitespace-subdued-space-after-tab ((((background dark)) (:inherit unicode-whitespace-subdued-empty)) (((background light)) (:inherit unicode-whitespace-subdued-empty))) "Unicode-whitespace face for 8 or more spaces after a tab." unicode-whitespace-subdued-space-before-tab ((((background dark)) (:inherit unicode-whitespace-subdued-empty)) (((background light)) (:inherit unicode-whitespace-subdued-empty))) "Unicode-whitespace face for space before tab at start of line." unicode-whitespace-subdued-tab ((((background dark)) (:foreground "DarkGray" :background "Gray20")) (((background light)) (:foreground "Black" :background "Gray80"))) "Unicode-whitespace face for tab characters." unicode-whitespace-subdued-indentation ((((background dark)) (:inherit unicode-whitespace-subdued-tab)) (((background light)) (:inherit unicode-whitespace-subdued-tab))) "Unicode-whitespace face for indentation." unicode-whitespace-subdued-space ((((background dark)) (:foreground "Gray30")) (((background light)) (:foreground "Gray70"))) "Unicode-whitespace face for soft space characters." unicode-whitespace-subdued-hspace ((((background dark)) (:foreground "Gray40")) (((background light)) (:foreground "Gray60"))) "Unicode-whitespace face for hard space characters." unicode-whitespace-subdued-newline ((((background dark)) (:inherit unicode-whitespace-subdued-space)) (((background light)) (:inherit unicode-whitespace-subdued-space))) "Unicode-whitespace face for newline characters." unicode-whitespace-subdued-line ((((background dark)) nil) (((background light)) nil)) "Unicode-whitespace face for long lines."] 6)
#@66 Major modes in which to enable newline visualization by default.
(custom-declare-variable 'unicode-whitespace-newline-mark-modes ''(indented-text-mode makefile-automake-mode makefile-bsdmake-mode makefile-gmake-mode makefile-imake-mode makefile-makepp-mode makefile-mode markdown-mode org-mode snippet-mode text-mode) '(#$ . 15309) :type '(repeat symbol) :group 'unicode-whitespace)
#@108 Which names to print when the 'echo style is in effect.

See `unicode-whitespace-toggle-echo' to activate.
(custom-declare-variable 'unicode-whitespace-echo-level 't '(#$ . 15699) :type '(choice (const :tag "Non-ASCII whitespace" non-ascii) (const :tag "All whitespace" t)) :group 'unicode-whitespace)
#@149 A backward-compatible version of `called-interactively-p'.

Optional KIND is as documented at `called-interactively-p'
in GNU Emacs 24.1 or higher.
(defalias 'unicode-whitespace-called-interactively-p '(macro . #[(&optional kind) "\301\302!\204 \303\207\304\305\306\217\203 \302D\207\307\207" [kind fboundp called-interactively-p (interactive-p) nil (byte-code "\300\301!\210\302\207" [called-interactively-p any t] 2) ((error)) (called-interactively-p)] 3 (#$ . 16008)]))
#@74 If the point is on whitespace, identify that character in the echo area.
(defalias 'unicode-whitespace-echo #[nil "\2032 `	=\2042 \305\306\n!*\2032 \f\307=\203& \310\305\306\n!*\2042 \311\312\313\314\315\316\317f!#\"\210`\211\207" [unicode-whitespace-all-whitespace-regexp unicode-whitespace-last-point regexp inhibit-changing-match-data unicode-whitespace-echo-level t looking-at non-ascii "[ 	\f]" message "%s" replace-regexp-in-string "\\`Character Tabulation" "Tab" ucs-utils-pretty-name nil] 7 (#$ . 16490)])
#@583 Enable newline marks by default only in certain cases.
Newline marked are enabled when `word-wrap' or `truncate-lines'
is set, or when the major mode of the buffer is listed in
`unicode-whitespace-newline-mark-modes'.

Newline marks can be toggled interactively with
`unicode-whitespace-toggle-newlines'.

Also (an unrelated bugfix) turn off `show-trailing-whitespace'
temporarily when `whitespace-mode' is on and restore the original
value for the buffer when turning off `whitespace-mode'.  This is
needed because show-trailing-whitespace overrides on the face for
trailing tabs.
(defalias 'unicode-whitespace-hook-func #[nil "\204 \306\301!\210\307\n\211\207\307\f\204\" \204\" >\205: 		\310\311\312\"\210\306\301!\210\313	!\314 \210\315 )\207" [whitespace-mode whitespace-active-style unicode-whitespace-saved-show-trailing show-trailing-whitespace word-wrap truncate-lines make-local-variable nil add-to-list whitespace-style newline-mark copy-sequence whitespace-display-char-off whitespace-display-char-on major-mode unicode-whitespace-newline-mark-modes] 3 (#$ . 17020)])
#@73 Configure `whitespace-mode' to recognize Unicode whitespace characters.
(defalias 'unicode-whitespace-recognize-extended-characters #[nil "\306\307\"\306\n\307\"\306\f\307\"\306\307\"\306 \307\"!\306\"\307\"#!R$!Q%\310	\311Q&\310\311Q'\310!\311Q(\310\311Q)\310\311Q*\310#\311Q+\310$\311Q,\310%\311Q-\312&\313Q.\312-\313Q/\312*\313Q0\312,\313.R1\314	$#\315\310	$#\316\317\2602&\320\312,\321\260.,\320QB3\322	$#\316\323\2604\324	$#\316\325\326\2605\327&\330\314$#\316\331\332\325	\311\260\f\333$#\316\334\312&\313\335\260	B6\310	$#\316\325\311\260\2117.\207" [unicode-whitespace-tab-names tabs unicode-whitespace-tab-set-names tab-sets unicode-whitespace-soft-space-names official-soft-spaces ucs-utils-string drop "[" "]" "\\(" "+\\)" "\\([" "]+\f*" "\f" "]*\\)$" "+" "\\{%d,\\}\\)" "^\\(\\([" "]*\n\\)+\\)" "^\\([" "\n" "]+\\)" "^" "*" "]\\{%d,\\}\\)" "[^" "^[" "]*" "[^\n]" unicode-whitespace-hard-space-names hard-spaces unicode-whitespace-pseudo-space-names pseudo-spaces unicode-whitespace-alternative-line-terminator-names alt-line-terms all-spaces all-soft-spaces tabs-class tab-sets-class pseudo-spaces-class official-soft-spaces-class hard-spaces-class alt-line-terms-class all-spaces-class all-soft-spaces-class whitespace-tab-regexp whitespace-space-regexp whitespace-hspace-regexp whitespace-space-before-tab-regexp whitespace-trailing-regexp whitespace-space-after-tab-regexp whitespace-empty-at-bob-regexp whitespace-empty-at-eob-regexp whitespace-indentation-regexp unicode-whitespace-all-whitespace-regexp] 12 (#$ . 18122)])
#@60 Configure `whitespace-mode' to display Unicode characters.
(defalias 'unicode-whitespace-display-extended-characters #[nil "\306\307\310\311	\"\312\f	&\306 \211!\203\212 !@\211 @\310\313 \211\"A@)\"\314\315#$%\211&\n\235\203a \316\317\320%\321\"!\306'()\307*\322)('#,\203a \323#&+\235\203l \324$$\325&\326\"\327#\320%\321\"\"EB,!A\211!\204 *\312,-\"\306 \211.\205\362 .@\211 @\310\330 \211\"A@)\"\314$%\211&/\235\203\275 \331$$\325&\326\"\325&\326\"\325\332\326\"=\203\333 \333\320%\321\"!\202\343 \333\320%\321\"\334\"EB+.A\211.\204\232 \306,\207" [whitespace-display-mappings unicode-whitespace-form-feed-mappings form-feed-names case-fold-search unicode-whitespace-soft-space-mappings unicode-whitespace-hard-space-mappings nil t mapcar car append #[(x) "G\301U\203 \302H\207\207" [x 1 0] 2] space-mark 1 "box drawing" ucs-utils-pretty-name ucs-utils-first-existing-char cdp string-match 20 tab-mark ucs-utils-char error make-vector #[(x) "G\301U\203 \302H\207\207" [x 1 0] 2] newline-mark "Carriage Return (CR)" vector 10 unicode-whitespace-pseudo-space-mappings unicode-whitespace-tab-mappings unicode-whitespace-tab-set-mappings cell #1=#:--cl-dolist-temp-- x display-len mark-type to-charnames from-charname start string regexp inhibit-changing-match-data unicode-whitespace-tab-names unicode-whitespace-standard-line-terminator-mappings unicode-whitespace-alternative-line-terminator-mappings #2=#:--cl-dolist-temp-- unicode-whitespace-standard-line-terminator-names] 8 (#$ . 19747)])
#@37 Configure `whitespace-mode' styles.
(defalias 'unicode-whitespace-configure-styles #[nil "\301\302\303\"\210\304\305\306\307\310$\210\311\305\310\"\210\312\313\314\"\210\315\211\207" [whitespace-style add-to-list whitespace-style-value-list echo ad-add-advice whitespace-post-command-hook (whitespace-post-command-hook-echo nil t (advice lambda nil "Identify the whitespace character under the point in the echo area." (when (memq 'echo whitespace-active-style) (unicode-whitespace-echo)))) after nil ad-activate add-hook whitespace-mode-hook unicode-whitespace-hook-func (empty face indentation newline space-after-tab::tab space-before-tab::tab space-mark spaces tab-mark tabs trailing)] 5 (#$ . 21318)])
#@127 Change the faces used by `whitespace-mode' to subdued coloring.

With negative prefix ARG, sets faces back to default values.
(defalias 'unicode-whitespace-subdued-faces #[(&optional arg) "\306!\307W\203# \301\302\303\304\305\310\311	\312\n\313\314\211\f\207\315\316\317\320\321\322\323	\324\n\325\326\211\f\207" [arg whitespace-empty whitespace-trailing whitespace-indentation whitespace-space-after-tab whitespace-space-before-tab prefix-numeric-value 0 whitespace-tab whitespace-space whitespace-hspace whitespace-newline whitespace-line unicode-whitespace-subdued-empty unicode-whitespace-subdued-trailing unicode-whitespace-subdued-indentation unicode-whitespace-subdued-space-after-tab unicode-whitespace-subdued-space-before-tab unicode-whitespace-subdued-tab unicode-whitespace-subdued-space unicode-whitespace-subdued-hspace unicode-whitespace-subdued-newline unicode-whitespace-subdued-line] 2 (#$ . 22033) "p"])
#@272 Configure `whitespace-mode' to be aware of extended characters.

This only needs to be run once per session.

When optional FACES is non-nil, change whitespace faces to
subdued coloring, on the theory that the new display glyphs
are sufficient to distinguish whitespace.
(defalias 'unicode-whitespace-setup #[(&optional subdued-faces) "\301 \210\302 \210\303 \210\205 \304\305!\207" [subdued-faces unicode-whitespace-recognize-extended-characters unicode-whitespace-display-extended-characters unicode-whitespace-configure-styles unicode-whitespace-subdued-faces 1] 2 (#$ . 22988) "P"])
#@53 Toggle `whitespace-mode' echo-area feedback on/off.
(defalias 'unicode-whitespace-toggle-echo #[nil "\303\300!\2052 \2052 \304\305\306\"\210\307\310\306	#\300\311!\210\300\312!\210\313\314!\2051 \315\316\306\n>\203/ \317\2020 \320\")\207" [whitespace-mode whitespace-active-style whitespace-style boundp add-to-list whitespace-style-value-list echo whitespace-toggle-list t 0 1 called-interactively-p interactive message "echo feedback %s" "enabled" "disabled"] 4 (#$ . 23583) nil])
#@53 Toggle `whitespace-mode' newline visibility on/off.
(defalias 'unicode-whitespace-toggle-newlines #[nil "\303\300!\205- \205- \304\305\306	#\300\307!\210\300\310!\210\311\312!\205, \313\314\306\n>\203* \315\202+ \316\")\207" [whitespace-mode whitespace-active-style whitespace-style boundp whitespace-toggle-list t newline-mark 0 1 called-interactively-p interactive message "newline marks %s" "enabled" "disabled"] 4 (#$ . 24074) nil])
(provide 'unicode-whitespace)
