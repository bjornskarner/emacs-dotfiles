;;; smex-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (smex-initialize smex) "smex" "smex.el" (20736
;;;;;;  11870))
;;; Generated autoloads from smex.el

(autoload 'smex "smex" "\


\(fn)" t nil)

(autoload 'smex-initialize "smex" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("smex-pkg.el") (20736 11870 392117))

;;;***

(provide 'smex-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; smex-autoloads.el ends here
